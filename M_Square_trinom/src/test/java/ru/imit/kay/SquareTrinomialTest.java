package ru.imit.kay;

import org.junit.Test;
import static org.junit.Assert.*;

public class SquareTrinomialTest{
 //https://github.com/junit-team/junit4/wiki/Assertions
   @Test
   public void TestNull() {
		SquareTrinomial all_zero, only_a_zero, complex_roots;

		all_zero=new SquareTrinomial(0, 0, 0);
		only_a_zero=new SquareTrinomial(0, 15, 21);
		complex_roots=new SquareTrinomial(5.2, 1, 0.32);
	   
		assertNull("All zero", all_zero.GetRoots());
		assertNull("Only a is zero", only_a_zero.GetRoots());
		assertNull("Has only complex roots ", complex_roots.GetRoots());
   }
   
   @Test
   public void SingleRoot() {
		SquareTrinomial single_pos, single_neg;
		double[] pos_ans, neg_ans;
		final double solve_pos=1.0, solve_neg=-.5, precision=1e-9;

		single_pos=new SquareTrinomial(2, -4, 2);
		single_neg=new SquareTrinomial(-8, -8, -2);
		pos_ans=single_pos.GetRoots();
		neg_ans=single_neg.GetRoots();
	   
		assertEquals("Single pos root. Not only one", pos_ans.length, 1);
		assertTrue("Single pos root. Must be 1.0", Math.abs(pos_ans[0]-solve_pos)<precision);
		assertEquals("Single neg root. Not only one", neg_ans.length, 1);
		assertTrue("Single neg root. Must be -0.5", Math.abs(neg_ans[0]-solve_neg)<precision);
   }
   
   @Test
   public void RegularRoots() {
		SquareTrinomial equation;
		double[] ans;
		//https://ru.onlinemschool.com/math/assistance/equation/quadratic/
		final double[] solves={-0.0265318830, 0.3779982059};
		final double precision=1e-9;

		equation=new SquareTrinomial(15.515, -5.453, -0.1556);
		ans=equation.GetRoots();
		
		assertEquals("Count of roots must be eq of two", ans.length, 2);
		assertTrue("Mismatch of the first root ", 
			Math.abs(ans[0]-solves[0])<precision);
		assertTrue("Mismatch of the second root ", 
			Math.abs(ans[1]-solves[1])<precision);
   }
}
