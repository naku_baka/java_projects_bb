package ru.imit.kay;

public class SquareTrinomial {
	private double a;
	private double b;
	private double c;
	
	public SquareTrinomial(double a, double b, double c)
	{
		this.a=a;
		this.b=b;
		this.c=c;
	}
	
	public double[] GetRoots()
	{
		if(a==0)
			return null;
		
		double D=b*b-4*a*c;
		
		if(D<0)
			return null;
		
		if(D==0)
			return new double[]{-b/(2*a)};
		
		D=Math.sqrt(D);
		return new double[]{(-b-D)/(2*a), (-b+D)/(2*a)};
	}
	
    public static void main(String[] argv) {
        System.out.println("Hello world");
    }
}
